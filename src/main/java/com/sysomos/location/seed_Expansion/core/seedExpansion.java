package com.sysomos.location.seed_Expansion.core;

import java.util.List;

import org.apache.spark.api.java.JavaPairRDD;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.sysomos.location.seed_Expansion.data.FetchAndSaveData;
import com.sysomos.location.seed_Expansion.utils.Pair;

public class seedExpansion {
	public static class Parameters {
		@Parameter(names = { "-p"}, required = false, description = "threshold for location probability")
		private Double probThreshold = 0.69;
		@Parameter(names = { "-s"}, required = false, description = "threshold for location num of seedsfriend")
		private Integer seedsFriendThreshold = 5;
	}
	public static void main(String args[]) {
		Parameters params = new Parameters();
		new JCommander(params, args).parse();
		JavaPairRDD<Long, List<Pair<String, Double>>> seeds = FetchAndSaveData.fetch(params.probThreshold, params.seedsFriendThreshold);
		FetchAndSaveData.save(seeds);
	}
}