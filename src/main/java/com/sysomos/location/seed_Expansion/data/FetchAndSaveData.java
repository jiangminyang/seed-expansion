package com.sysomos.location.seed_Expansion.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.hive.HiveContext;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import com.sysomos.location.seed_Expansion.utils.Pair;

import scala.Tuple2;

public class FetchAndSaveData {
	static String APP_NAME = "location.geo_inference.seedExpansion";
	static SparkConf sparkConf;
	static JavaSparkContext sparkContext;
	static HiveContext hiveContext;
	static final int NUM_TASKS = 150;

	static {
		sparkConf = new SparkConf().setAppName(APP_NAME);
		sparkContext = new JavaSparkContext(sparkConf);
		sparkContext.hadoopConfiguration().set("mapreduce.input.fileinputformat.input.dir.recursive", "true");
		hiveContext = new HiveContext(sparkContext.sc());
	//	System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
	}

	public static JavaPairRDD<Long, List<Pair<String, Double>>> fetch(Double probThreshold, Integer seedsFriendThreshold) {
		System.out.println("Start fetching fellower geo data from hive at " + (new Date()).toString());
		hiveContext.sql("use lookup_tables");
		JavaPairRDD<Long, List<Pair<String, Double>>> result = hiveContext
				.sql("select id, place from user_inferred_geo_table where probability >= " + probThreshold + " and numofseedfriends >=" + seedsFriendThreshold).javaRDD()
				.mapToPair(new PairFunction<Row, Long, List<Pair<String, Double>>>() {

					public Tuple2<Long, List<Pair<String, Double>>> call(Row row) throws Exception {
						Long id = row.getLong(0);
						List<Row> places = row.getList(1);
						List<Pair<String, Double>> placesArr = new ArrayList<Pair<String, Double>>();
						for (int i = 0; i < places.size(); i++) {
							placesArr.add(
									new Pair<String, Double>(places.get(i).getString(0), places.get(i).getDouble(1)));
						}
						return new Tuple2<Long, List<Pair<String, Double>>>(id, placesArr);
					}

				});
		System.out.println("Finish fetching fellower geo data from hive at " + (new Date()).toString());
		return result;
	}

	public static void save(JavaPairRDD<Long, List<Pair<String, Double>>> data) {
		System.out.println("Start saving expanded seeds into seedsTable at " + (new Date()).toString());
		hiveContext.sql("use lookup_tables");
		List<StructField> fields = new ArrayList<StructField>();
		List<StructField> loc = new ArrayList<StructField>();
		loc.add(DataTypes.createStructField("key", DataTypes.StringType, false));
		loc.add(DataTypes.createStructField("value", DataTypes.DoubleType, false));

		fields.add(DataTypes.createStructField("id", DataTypes.LongType, false));
		fields.add(DataTypes.createStructField("location", DataTypes.StringType, false));
		fields.add(DataTypes.createStructField("probability", DataTypes.DoubleType, false));
		fields.add(DataTypes.createStructField("place", DataTypes.createArrayType(DataTypes.createStructType(loc)),
				false));
		fields.add(DataTypes.createStructField("original", DataTypes.IntegerType, false));
		StructType seedSchema = DataTypes.createStructType(fields);
		JavaRDD<Row> seeds = data.map(new Function<Tuple2<Long, List<Pair<String, Double>>>, Row>() {

			public Row call(Tuple2<Long, List<Pair<String, Double>>> row) throws Exception {
				double max = 0.0;
				String loc = "";
				List<Row> place = new ArrayList<Row>();
				List<Pair<String, Double>> temp = row._2;
				for (int i = 0; i < temp.size(); i++) {
					place.add(RowFactory.create(temp.get(i).getA(), temp.get(i).getB()));
					if (temp.get(i).getB() > max) {
						max = temp.get(i).getB();
						loc = temp.get(i).getA();
					}
				}
				return RowFactory.create(row._1, loc, max, place, 0);
			}

		});
		JavaRDD<Row> originalSeeds = hiveContext.sql("select * from seedstable").javaRDD().filter(new Function<Row, Boolean>() {

			public Boolean call(Row row) throws Exception {
				return row.getInt(4) == 1;
			}
			
		});
		seeds = seeds.union(originalSeeds);
		DataFrame df = hiveContext.createDataFrame(seeds, seedSchema);
		df.write().mode(SaveMode.Overwrite).saveAsTable("seedsTable_temp");
		hiveContext.sql("drop table seedstable");
		hiveContext.sql("ALTER TABLE seedsTable_temp RENAME TO seedsTable");
		System.out.println("Finish saving expanded seeds into seedsTable at " + (new Date()).toString());
		sparkContext.close();
	}
}